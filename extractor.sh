#!/bin/sh
#****************************************************************#
# ScriptName: extractor.sh
# Author: $SHTERM_REAL_USER@alibaba-inc.com
# Create Date: 2022-08-05 10:37
# Modify Author: $SHTERM_REAL_USER@alibaba-inc.com
# Modify Date: 2022-08-19 16:54
# Function: 
#***************************************************************#

#!/bin/bash

# Download directories vars
root_dl="k400"
root_dl_targz="Kinetics400"

# Make root directories
[ ! -d $root_dl ] && mkdir $root_dl

# Extract train
curr_dl=$root_dl_targz/train
curr_extract=$root_dl/train_before
[ ! -d $curr_extract ] && mkdir -p $curr_extract
tar_list=$(ls $curr_dl)
for f in $tar_list
do
	[[ $f == *.tar ]] && echo Extracting $curr_dl/$f to $curr_extract && tar -xzxf $curr_dl/$f -C $curr_extract
done

# Extract validation
curr_dl=$root_dl_targz/val
curr_extract=$root_dl/val
[ ! -d $curr_extract ] && mkdir -p $curr_extract
tar_list=$(ls $curr_dl)
for f in $tar_list
do
	[[ $f == *.tar ]] && echo Extracting $curr_dl/$f to $curr_extract && tar -xzxf $curr_dl/$f -C $curr_extract
done

# Extraction complete
echo -e "\nExtractions complete!"
