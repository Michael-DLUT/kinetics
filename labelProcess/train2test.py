#!/usr/bin/python
#****************************************************************#
# ScriptName: labelProcess.py
# Author: $SHTERM_REAL_USER@alibaba-inc.com
# Create Date: 2022-08-08 10:33
# Modify Author: $SHTERM_REAL_USER@alibaba-inc.com
# Modify Date: 2022-08-09 10:01
# Function: 
#***************************************************************#
import pandas as pd

data = pd.read_csv('train.list',names=['path','class'],sep=' ')
data['path']= "/root/dataset/k400/train/"+data['path']        
data.to_csv('../test.csv',index=False,header=False,sep=' ') 

#data = pd.read_csv('val.list',names=['path','class'],sep=' ')
#data['path']= "/root/dataset/k400/val/"+data['path']        
#data.to_csv('val.csv',index=False,header=False,sep=' ') 
#data.to_csv('test.csv',index=False,header=False,sep=' ') 
