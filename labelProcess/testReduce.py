#!/usr/bin/python
#****************************************************************#
# ScriptName: labelProcess.py
# Author: $SHTERM_REAL_USER@alibaba-inc.com
# Create Date: 2022-08-08 10:33
# Modify Author: $SHTERM_REAL_USER@alibaba-inc.com
# Modify Date: 2022-08-29 21:15
# Function: 
#***************************************************************#
import pandas as pd

data_before = pd.read_csv('test_origin.csv',names=['path','class'],sep=' ')
#print(data_before.shape)
#print(type(data_before))
#print(type(data_before['class']))
#print(list(data_before['class']).index(1))
#print(type(data_before['class'][0]))
data_list = []
for label in range(400):
    index_start = list(data_before['class']).index(label)
    for i in range(5):
        data_list.append([data_before['path'][index_start+i], data_before['class'][index_start+i]])
name = ['path', 'class']
data = pd.DataFrame(columns=name, data=data_list)
print(data.shape)
data.to_csv('test.csv',index=False,header=False,sep=' ') 
