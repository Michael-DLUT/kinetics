#!/bin/sh
#****************************************************************#
# ScriptName: download.sh
# Author: $SHTERM_REAL_USER@alibaba-inc.com
# Create Date: 2022-08-04 20:11
# Modify Author: $SHTERM_REAL_USER@alibaba-inc.com
# Modify Date: 2022-08-04 20:11
# Function: 
#***************************************************************#
file=$1

while read line 
do
	wget "$line"
done <$file

